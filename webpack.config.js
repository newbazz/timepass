const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const {InjectManifest} = require('workbox-webpack-plugin');

const html_files = ['index','pre-test-quiz','pre-test','bubble-sort-experiment','recap','bubble-sort','basic-concept','bubble-sort-algorithm','bubble-sort-demo','bubble-sort-practice','bubble-sort-exercise','bubble-sort-quiz','optimized-bubble-sort','optimization-technique','optimized-bubble-sort-demo','optimized-bubble-sort-practice','optimized-bubble-sort-exercise','optimized-bubble-sort-quiz','analysis','time-and-space-complexity','stability-of-bubble-sort','comparison-with-other-algorithms','analysis-quiz','post-test','post-test-quiz','feedback'];


module.exports = {
	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'main.js'
	},
	externals: {
		jquery: 'jQuery'
	},
	module:{
		rules:[
			{
				test:/\.css$/,
				loader: ['style-loader','css-loader']
			},
			{
				test: /\.(png|jpe?g|gif)$/i,
				use: [
						{
							loader: 'file-loader',
							options: {
								name: 'images/[name].[ext]',
							}
						},
					],
			},
			{
				test: /\.exec\.js$/,
				use: [
					{
							loader: 'babel-loader',
							options: {
								name: '[name].[ext]',
								"presets": [
									['es2015', {modules: false}]
								],
							}
						},
				]
			},
			{
				test: /\.html$/,
				use: ['html-loader'],
			},
		]
	},
	plugins:[
        new CopyWebpackPlugin([
            {from:'src/code',to:'./code'} 
        ]),
		...html_files.map(f => 
        	new HtmlWebpackPlugin({
				filename: f.concat('.html'),
				template: 'src/'.concat(f).concat('.html'),
				inject: 'head',
				attributes: false,
		})),
		new InjectManifest({
			swSrc: './src/service-worker.js',
			swDest: 'service-worker.js',
			exclude: [
				/\.map$/,
				/manifest$/,
				/\.htaccess$/,
				/service-worker\.js$/,
				/sw\.js$/,
			],
		}),
	]
}