import './quiz-styles/css/quiz.css'
import './artefacts-styles/css/artefacts.css'
import './content-styles/css/content.css'
import './toc-styles/css/toc.css'
import './header-styles/css/header.css'
import './common-styles/css/common.css'

var getTocHeaderDiv = function() {
  return getObjByClassName(getTocDiv(), 'toc-header');
};

var getExpPreambleDiv = function() {
  return getObjByClassName(getTocDiv(), 'exp-preamble');
};

function getLusUl() {
  return getObjByClassName(getTocDiv(), 'toc-lu-ul');
};

function getLusLi() {
  return getObjsByClassName(getLusUl(), 'toc-lu-li');
};

function getLuTasksUl(obj) {
  return getObjByClassName(obj, 'toc-tsk-ul');
};

function getLuQuizzesUl(obj) {
  return getObjByClassName(obj, 'toc-quiz-ul');
};  

function getTasksUl() {
  return getObjsByClassName(getLusUl(), 'toc-tsk-ul');
};

function getQuizzesUl() {
  return getObjsByClassName(getLusUl(), 'toc-quiz-ul');
};
function navigate(path) {
  var current = window.location.href;
  window.location.href = current.replace(/#(.*)$/, '') + '#' + path;
};
function collapseLearningUnits(){
    var aTag = document.getElementsByTagName('a');
    for(var i=0;i<aTag.length;i+=1){
      var span = aTag[i].getElementsByTagName('span');
      for(var j in span){
        if(span[j].className=="glyphicon glyphicon-triangle-bottom"){
          span[j].addEventListener('click', function(event) {
            toggleLearningUnits(aTag);
            event.stopPropagation();
          });
        }
      }
    }
  }

var makeArray = function(obj) {
  return Array.from(obj);
};
var getObjsByClassName = function(obj, cls) {
  return makeArray(obj.getElementsByClassName(cls));
};

var getObjByClassName = function(obj, cls) {
  var clsObjs = getObjsByClassName(obj, cls);
  return clsObjs.filter(clsObj => {
    return (clsObj.parentElement.className == obj.className ||
            clsObj.parentElement.className == "");
  })[0];                       
};


var getWrapperDiv = () => {
  return getObjByClassName(document, "wrapper");
};
var getTocDiv = () => {
  return getObjByClassName(getWrapperDiv(), "toc");
};


var toggleLearningUnits = function(el) {
  // console.log(el)
  $(el).siblings().collapse('toggle');
};
var registerOnClickHandler = function() {
  var tocDiv = getTocDiv();
  var tocEls = makeArray(tocDiv.getElementsByTagName('a'));
  tocEls.forEach(function(tocEl){
    var currentHash = tocEl.getAttribute('href').replace('.html', '');
    // console.log(currentHash)
    tocEl.addEventListener('click', function(event) {
      // changeState(currentHash);
      toggleLearningUnits(event.target);
    });
  });
};
var changeState = function(hashPath) {
  var els = makeArray(document.getElementsByTagName('*'));
  console.log(els[0]['title']);
  els.forEach(function(el) {
    if(el['title'] === hashPath) {
      if(isExp(el) || isLu(el) || isTask(el)) 
        artfctsRenderer(el);
      if(isQuiz(el)) 
        quizRenderer(el);
    }
  });
  if(hashPath === 'feedback') 
      feedbackRenderer();
};
$(document).ready(function() {
  // registerOnClickHandler();
  collapseLearningUnits();
  var el = document.getElementsByClassName('toc-active-el');
  $(el[0]).siblings().collapse('show');

});

